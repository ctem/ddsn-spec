# Changelog

All notable changes to the [DDSN Design Specification][ddsn] are documented in this file.

The format is based on [Keep a Changelog] with entry subheadings ordered as follows: `Packaging`, `Added`, `Changed`, `Fixed`, `Removed`.

This project adheres to [Semantic Versioning].

[ddsn]: https://codeberg.org/ctem/ddsn-spec
[keep a changelog]: https://keepachangelog.com/en/1.0.0/
[semantic versioning]: https://semver.org/spec/v2.0.0.html

## [0.4.0](https://codeberg.org/ctem/ddsn-spec/compare/v0.4.0...v0.3.0) - 2021-05-15

### Added

- Sample Standards appendix
- Sample Membership Tables appendix
- Standards Template appendix
- Membership Tables Template appendix

## [0.3.0](https://codeberg.org/ctem/ddsn-spec/compare/v0.3.0...v0.2.0) - 2021-04-24

### Added

- Specification section
- Mesh VPN section
- DDS section
- SDS section
- Orgs section
- Groups section
- Participants section
- Participant Configurations section
- BSDS Participant Configs section
- GSDS Participant Configs section
- BR section
- GR section

## [0.2.0](https://codeberg.org/ctem/ddsn-spec/compare/v0.2.0...v0.1.0) - 2021-03-27

### Added

- Glossary section
- Membership Glossary section
- Storage Glossary section

## [0.1.0](https://codeberg.org/ctem/ddsn-spec/compare/v0.1.0...v0.0.0) - 2021-03-20

### Packaging

- **License:** add a copy of CC BY-SA 4.0
- **README:** add repository documentation
- Add Git attributes file
- Add EditorConfig file

### Added

- Documentation structure outline
- Introduction section

## [0.0.0](https://codeberg.org/ctem/ddsn-spec/releases/tag/v0.0.0) - 2021-03-13

### Packaging

- Initialize empty repository
