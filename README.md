# Distributed Data Store Network (DDSN) Design Specification

## License

Content is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International][cc] license (SPDX ID `CC-BY-SA-4.0`). A copy is included in the [COPYING] file.

[cc]: https://creativecommons.org/licenses/by-sa/4.0/
[copying]: COPYING
