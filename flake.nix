{
  description = "Distributed Data Store Network (DDSN) Design Specification";

  inputs.fu.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, fu }:
    fu.lib.eachSystem [ "aarch64-linux" "x86_64-linux" ]
      (
        system:
        let
          pkgs = nixpkgs.legacyPackages.${system};
          deps = with pkgs; [ mdbook ];
        in
        rec
        {
          devShell = pkgs.mkShell { buildInputs = deps; };
        }
      );
}
