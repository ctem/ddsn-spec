# Contents in Brief

[Introduction](./introduction.md)

- [Glossary](./glossary/README.md)
  - [Membership](./glossary/membership.md)
  - [Storage](./glossary/storage.md)
- [Specification](./specification/README.md)
  - [Mesh VPN](./specification/mesh-vpn.md)
  - [DDS](./specification/dds.md)
  - [SDS's](./specification/sds.md)
  - [Orgs](./specification/orgs.md)
  - [Groups](./specification/groups.md)
  - [Participants](./specification/participants.md)
  - [Participant Configurations](./specification/participant-configs/README.md)
    - [BSDS](./specification/participant-configs/bsds.md)
    - [GSDS](./specification/participant-configs/gsds.md)
  - [BR's](./specification/br.md)
  - [GR's](./specification/gr.md)

# Appendix A: Sample Implementation Reference

- [Standards](appendices/samples/standards.md)
- [Membership Tables](appendices/samples/membership-tables.md)

# Appendix B: Implementation Reference Templates

- [Standards](appendices/templates/standards.md)
- [Membership Tables](appendices/templates/membership-tables.md)
