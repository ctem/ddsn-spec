# Sample Implementation Reference: Membership Tables

The following tables outline membership details of all components of the _Sample Collective_ DDSN implementation as internally standardized.

Notes:

- By default, all users are BSDS visitors. As this is a given, the associated `bv.` subcolumn (with its consistent `v-alice` `v-bob` value on every row) of the `org` supercolumn is excluded from all tables.
- For clarity, source (i.e., non-generated) names are used for participants and orgs in the table.

Key:

- column subheaders
  - `bm.`: BSDS maintainers (NOTE: `m-` prefix removed from user name for brevity)
  - `bc.`: BSDS contributors (NOTE: `c-` prefix removed from user name for brevity)
  - _`bv.`: BSDS visitors (removed for brevity; see note)_
  - `gv.`: GSDS visitors (NOTE: `v-` prefix removed from user name for brevity)
  - `k.`: BR keyholders
  - `n.`: name

## Single-administrator hosts

Hosts administered by Alice:

| >             | >             | org           | GR          | >           | BR         |
| ------------- | ------------- | ------------- | ----------- | ----------- | ---------- |
| **bm. & bc.** | **gv.**       | **n.**        | **n.**      | **n.**      | **k.**     |
| `alice`       | `alice`       | `alicea`      | (root)      | `aggregate` | Alice      |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | `alice` `bob` | `aliceb`      | (root)      | `aggregate` | Alice, Bob |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| `alice` `bob` | ^             | `aliceandbob` | (root)      | `aggregate` | ^          |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | ^             | `thirdparty`  | (root)      | `root`      | ^          |
| ^             | ^             | ^             | `audio`     | `audio`     | ^          |
| ^             | ^             | ^             | `documents` | `documents` | ^          |
| ^             | ^             | ^             | `images`    | `images`    | ^          |
| ^             | ^             | ^             | `state`     | `state`     | ^          |
| ^             | ^             | ^             | `vault`     | `vault`     | ^          |
| ^             | ^             | ^             | `videos`    | `videos`    | ^          |
| `bob`         | ^             | `bobb`        | (root)      | `aggregate` | ^          |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | -             | `boba`        | -           | `aggregate` | Bob        |

Hosts administered by Bob:

| >             | >             | org           | GR          | >           | BR         |
| ------------- | ------------- | ------------- | ----------- | ----------- | ---------- |
| **bm. & bc.** | **gv.**       | **n.**        | **n.**      | **n.**      | **k.**     |
| `bob`         | `bob`         | `boba`        | (root)      | `aggregate` | Bob        |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | `alice` `bob` | `bobb`        | (root)      | `aggregate` | Alice, Bob |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| `alice` `bob` | ^             | `aliceandbob` | (root)      | `aggregate` | ^          |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | ^             | `thirdparty`  | (root)      | `root`      | ^          |
| ^             | ^             | ^             | `audio`     | `audio`     | ^          |
| ^             | ^             | ^             | `documents` | `documents` | ^          |
| ^             | ^             | ^             | `images`    | `images`    | ^          |
| ^             | ^             | ^             | `state`     | `state`     | ^          |
| ^             | ^             | ^             | `vault`     | `vault`     | ^          |
| ^             | ^             | ^             | `videos`    | `videos`    | ^          |
| `alice`       | ^             | `aliceb`      | (root)      | `aggregate` | ^          |
| ^             | ^             | ^             | `audio`     | ^           | ^          |
| ^             | ^             | ^             | `documents` | ^           | ^          |
| ^             | ^             | ^             | `images`    | ^           | ^          |
| ^             | ^             | ^             | `state`     | ^           | ^          |
| ^             | ^             | ^             | `vault`     | ^           | ^          |
| ^             | ^             | ^             | `videos`    | ^           | ^          |
| ^             | -             | `alicea`      | -           | `aggregate` | Alice      |

## Multiple-administrator hosts

Hosts administered by Alice and Bob:

| >             | org           | >           | BR         |
| ------------- | ------------- | ----------- | ---------- |
| **bm. & bc.** | **n.**        | **n.**      | **k.**     |
| `alice`       | `alicea`      | `aggregate` | Alice      |
| `bob`         | `boba`        | `aggregate` | Bob        |
| `alice`       | `aliceb`      | `aggregate` | Alice, Bob |
| `bob`         | `bobb`        | `aggregate` | ^          |
| `alice` `bob` | `aliceandbob` | `aggregate` | ^          |
| ^             | `thirdparty`  | `root`      | ^          |
| ^             | ^             | `audio`     | ^          |
| ^             | ^             | `documents` | ^          |
| ^             | ^             | `images`    | ^          |
| ^             | ^             | `state`     | ^          |
| ^             | ^             | `vault`     | ^          |
| ^             | ^             | `videos`    | ^          |
