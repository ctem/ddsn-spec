# Sample Implementation Reference: Standards

This document outlines the internal DDSN implementation standards of _Sample Collective_.

## Mesh VPN

- Implementation: default
- Name generation template: `ddsn`
- Generated name: `qojvosepo`

## DDS

- Deployment path: default
- Bind-mounted directories: default

## SDS's

- Archive-designated SDS implementation: default
- Work-designated SDS implementation: default

## Orgs

- Implementation: IPC-strict
- Name generation procedure:

  1. Generate name from template `<source org name (excluding final alphabetical enumeration)>.dds` using maintainer-shared [Spectre] “account”.
     - E.g., `alice.dds` → `bemciwepo`
  1. Replace first letter of generated name with first letter of source org name.
     - E.g., `bemciwepo` → `aemciwepo`
  1. Replace last letter of generated name with alphabetical enumeration.
     - E.g., `aemciwepo` → `aemciwepa`, `aemciwepb`, etc.

All initially standardized org names are collected in following table.

| >             | org name       |
| ------------- | -------------- |
| **source**    | **generated**  |
| `alicea`      | `aemciwepa`    |
| `aliceb`      | `aemciwepb`    |
| `boba`        | `bagwecaxa`    |
| `bobb`        | `bagwecaxb`    |
| `aliceandbob` | `aicbuxega`    |
| >             | `thirdparty`\* |

\* denotes special org that retains source name.

## Groups

- Provisioned group type names and associated SDS implementation(s): defaults

## Participants

[Nix]-specific note: participant names invariably end up in the Nix store as authorized keys are collected there for each DDS user (e.g., `dds-borg-c-alice-authorized_keys`). Therefore, generated names are provisioned.

- Name generation procedure:

  1. Generate name from template `<source participant alias>@dds` using private [Spectre] “account”.
     - E.g., `alice@dds` → `votkuxofu`
  2. Replace first letter of generated name with first letter of source participant alias.
     - E.g., `votkuxofu` → `aotkuxofu`

All initially standardized participant aliases (to be included in the names of all PUA's provisioned to the respective participant) are collected in the following table.

| >          | participant alias |
| ---------- | ----------------- |
| **source** | **generated**     |
| `alice`    | `aotkuxofu`       |
| `bob`      | `bevragoza`       |

## Participant configurations

Membership assignment notes:

- Membership to each group is determined with respect to its corresponding org, which in turn was created in strict conformance to the IPC standard; any apparent redundancy (e.g., many groups appearing to have a common user list) is purely coincidental.
- For clarity, source (i.e., non-generated) participant aliases are used for user names in the tables.

### BSDS

| org           | BR keyholders | maintainers       | contributors      | visitors          |
| ------------- | ------------- | ----------------- | ----------------- | ----------------- |
| `alicea`      | Alice         | `m-alice`         | `c-alice`         | `v-alice` `v-bob` |
| `boba`        | Bob           | `m-bob`           | `c-bob`           | ^                 |
| `aliceb`      | Alice, Bob    | `m-alice`         | `c-alice`         | ^                 |
| `bobb`        | ^             | `m-bob`           | `c-bob`           | ^                 |
| `aliceandbob` | ^             | `m-alice` `m-bob` | `c-alice` `c-bob` | ^                 |
| `thirdparty`  | ^             | ^                 | ^                 | ^                 |

### GSDS

- Host-specific GSDS contents for Alice:

  | org           | visitors          |
  | ------------- | ----------------- |
  | `alicea`      | `v-alice`         |
  | `aliceb`      | `v-alice` `v-bob` |
  | `bobb`        | ^                 |
  | `aliceandbob` | ^                 |
  | `thirdparty`  | ^                 |

- Host-specific GSDS contents for Bob:

  | org           | visitors          |
  | ------------- | ----------------- |
  | `boba`        | `v-bob`           |
  | `aliceb`      | `v-alice` `v-bob` |
  | `bobb`        | ^                 |
  | `aliceandbob` | ^                 |
  | `thirdparty`  | ^                 |

## GR's

All standardized GR names recommended for inclusion in an applicable org are collected in the following list.

- `audio`
- `documents`
- `images`
- `state`: special “home state” GR
- `videos`
- `vault`

## BR's

Names of BR's correspond with those of their respective GR's with the following standardized exceptions:

- `aggregate`: contains multiple GR's
- `root`: contains a special “top-level” GR

[nix]: https://nixos.org/
[spectre]: https://spectre.app/
