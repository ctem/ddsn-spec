# Implementation Reference Template: Membership Tables

The following Markdown-formatted template may be used to generate a membership tables reference for a DDSN implementation. A [sample membership tables reference](../samples/membership_tables.md) is available in Appendix A of this Specification.

```markdown
# Implementation Reference: Membership Tables

The following tables outline membership details of all components of the \_\_\_ DDSN implementation as internally standardized.

Notes:

- By default, all users are BSDS visitors. As this is a given, the associated `bv.` subcolumn (with its consistent value on every row) of the `org` supercolumn is excluded from all tables.
- For clarity, source (i.e., non-generated) names are used for participants and orgs in the table.

Key:

- column subheaders
  - `bm.`: BSDS maintainers (NOTE: `m-` prefix removed from user name for brevity)
  - `bc.`: BSDS contributors (NOTE: `c-` prefix removed from user name for brevity)
  - _`bv.`: BSDS visitors (removed for brevity; see note)_
  - `gv.`: GSDS visitors (NOTE: `v-` prefix removed from user name for brevity)
  - `k.`: BR keyholders
  - `n.`: name

## Single-administrator hosts

Hosts administered by \_\_\_:

| >             | >       | org    | GR     | >      | BR     |
| ------------- | ------- | ------ | ------ | ------ | ------ |
| **bm. & bc.** | **gv.** | **n.** | **n.** | **n.** | **k.** |

## Multiple-administrator hosts

Hosts administered by \_\_\_ and \_\_\_:

| >             | org    | >      | BR     |
| ------------- | ------ | ------ | ------ |
| **bm. & bc.** | **n.** | **n.** | **k.** |
```
