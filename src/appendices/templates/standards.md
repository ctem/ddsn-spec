# Implementation Reference Template: Standards

The following Markdown-formatted template may be used to generate a standards reference for a DDSN implementation. A [sample standards reference](../samples/standards.md) is available in Appendix A of this Specification.

```markdown
# Implementation Reference: Standards

This document outlines the internal DDSN implementation standards of \_\_\_ .

## Mesh VPN

- Implementation:
- Name generation template/procedure:
- Generated name:

## DDS

- Deployment path:
- Bind-mounted directories:

## SDS's

- Archive-designated SDS implementation:
- Work-designated SDS implementation:

## Orgs

- Implementation:
- Name generation template/procedure:

All initially standardized org names are collected in following table.

| >          | org name      |
| ---------- | ------------- |
| **source** | **generated** |

\* denotes special org that retains source name.

## Groups

- Provisioned group type names and associated SDS implementation(s):

## Participants

- Name generation template/procedure:

All initially standardized participant aliases (to be included in the names of all PUA's provisioned to the respective participant) are collected in the following table.

| >          | participant alias |
| ---------- | ----------------- |
| **source** | **generated**     |

## Participant configurations

Membership assignment notes:

- Membership to each group is determined with respect to its corresponding org, which in turn was created in strict conformance to the IPC standard; any apparent redundancy (e.g., many groups appearing to have a common user list) is purely coincidental.
- For clarity, source (i.e., non-generated) participant aliases are used for user names in the tables.

### BSDS

| org | BR keyholders | maintainers | contributors | visitors |
| --- | ------------- | ----------- | ------------ | -------- |

### GSDS

- Host-specific GSDS contents for \_\_\_:

  | org | visitors |
  | --- | -------- |

## GR's

All initially standardized GR names recommended for inclusion in an applicable org are collected in the following list.

- ``

## BR's

Names of BR's correspond with those of their respective GR's with the following standardized exceptions:

- ``:
```
