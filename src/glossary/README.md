# Glossary

The glossary defines terms and acronyms used in this Specification, divided into categories for usage context.
