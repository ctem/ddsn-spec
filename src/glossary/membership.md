# Membership Glossary

<dl>

**<dh>administrative user account (AUA)</dh>**

<dd>a user account provisioned to a given participant for administrative operations on a given host within a given DDSN</dd>
<dd>NOTE: AUA's are not DDS-provisioned accounts; they are presumably members of <code>user</code> and <code>wheel</code> groups on their respective hosts</dd>
<dd>NOTE: In contrast with PUA's, AUA's are not explicitly shared among hosts</dd>

**<dh>contributor</dh>**

<dd>a PUA with membership to a given group of type <code>contributors</code></dd>

**<dh>intended participant configuration (IPC)</dh>**

<dd>a participant configuration assigned to an org (or BSDS org in the case of a BSDS/GSDS org pair) as a deliberate separation from that of all others</dd>

**<dh>maintainer</dh>**

<dd>a PUA with membership to a given group of type <code>maintainers</code></dd>

**<dh>organization (org)</dh>**

<dd>a directory assigned with ACLs (corresponding to a presumably unique participant configuration) within a given SDS</dd>
<dd>NOTE: Only explicitly included PUA's may access the GR's and BR's of a given org</dd>

**<dh>participant</dh>**

<dd>a sentient—presumably human—being to whom a set of one or more PUA's is provisioned for exclusive use</dd>
<dd>NOTE: In a default implementation, a typical participant is additionally a holder of one or more BR keys</dd>
<dd>NOTE: By default, no assumptions are made regarding whether a given participant is additionally an administrator of any peer in a given DDSN</dd>

**<dh>participant configuration</dh>**

<dd>an arrangement of PUA's (and BR key distribution where applicable) of one or more participants associated with a given org (and presumably all repositories contained within)</dd>

**<dh>participant user account (PUA)</dh>**

<dd>a user account provisioned to a given participant for dedicated DDS (i.e., non-administrative) operations in accordance with group membership on all applicable hosts within a given DDSN</dd>
<dd>NOTE: In contrast with AUA's, PUA's are explicitly shared among hosts</dd>

**<dh>visitor</dh>**

<dd>a PUA with membership to a given group of type <code>visitors</code></dd>

</dl>
