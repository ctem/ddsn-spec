# Storage Glossary

**<dh>archive-designated SDS</dh>**

<dd>an SDS abstractly dedicated to a given archival repository format</dd>
<dd>NOTE: Typically provisioned on a headless server such as a NAS unit or VPS</dd>

**<dh>Borg SDS (BSDS)</dh>**

<dd>an SDS provisioned to store BR's</dd>
<dd>NOTE: Implemented in this document as an archive-designated SDS</dd>

**<dh>BR</dh>**

<dd>a <a href="https://www.borgbackup.org/">Borg</a> repository stored in the root of a given org of a given BSDS of a given DDS</dd>

**<dh>distributed data store (DDS)</dh>**

<dd>a top-level chroot jail availing SSH-over-mesh-VPN-based access (as defined in this Specification) to all SDS-divided, org-moderated, repository-stored data contained within</dd>

**<dh>distributed data store network (DDSN)</dh>**

<dd>a mesh virtual private network (VPN) of hosts that each serve a DDS for PUA-authenticated access (as defined in this Specification) from each other host</dd>

**<dh>Git SDS (GSDS)</dh>**

<dd>an SDS provisioned to store GR's</dd>
<dd>NOTE: Implemented in this document as a work-designated SDS</dd>

**<dh>GR</dh>**

<dd>a <a href="https://git-scm.com/">Git</a> repository, presumably with an <a href="https://git-annex.branchable.com/">annex</a>, stored within a given org of a given GSDS of a given DDS</dd>
<dd>NOTE: The requirements for a GR to (1) have an annex and (2) be stored in the root of an org are tentatively excluded to extend allowed use of the abbreviation to submodules)</dd>

**<dh>sub-data store (SDS)</dh>**

<dd>a directory deployed to the root of a DDS on a given host, provisioned (from virtually any source, though typically a locally mounted drive) exclusively to store org-moderated repositories of a given format</dd>

**<dh>work-designated SDS</dh>**

<dd>an SDS abstractly dedicated to a given working tree-included repository format</dd>
<dd>NOTE: Typically provisioned on a workstation</dd>
