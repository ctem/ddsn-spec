# Introduction

Described herein is an alternative approach to cloud-based data storage, backup, and sharing that emphasizes cooperation—and encourages collaboration—within groups of people who know each other. Leveraging existing technologies created and maintained in the same spirit of cooperation and human welfare, a given **distributed data store network (DDSN)** enables a form of social computing that is unreliant on the web and thus offers its users (or [_participants_](./glossary/membership.html) in DDSN terminology) an exit from the current trend of ubiquitous reliance on mainstream byproducts of corporate hegemony.

## License

Content is licensed under the [Creative Commons Attribution-ShareAlike 4.0 International][cc] license.

[cc]: https://creativecommons.org/licenses/by-sa/4.0/
