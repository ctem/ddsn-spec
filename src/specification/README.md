# Specification

This Specification defines the structure of a given DDSN, providing a bottom-up overview of its components, as well as recommended defaults for a secure implementation. The included tables map high-level DDS concepts to access control lists (ACL's) applied to directory structures deployable to a given set of interconnected hosts accessible over SSH.

Each host is assumed to be provisioned as both a client and a server for SSH-based DDS operations. As DDS's are commonly deployed to workstations, ACL's are chosen carefully to provide a secure environment over SSH without impairing local interactive session UX for host administrators (via AUA's), who frequently will be working out of the same directories.
