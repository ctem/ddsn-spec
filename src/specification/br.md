# BR's

For any given BSDS-contained BR, one or more GSDS-contained GR's contain a corresponding git-annex special remote to read its archives. Creation of archives is an independent operation. Each archive of a specified Borg “subdir” is expected to contain a complete copy of the corresponding GR in its state at the time the archive was created.
