# DDS

Assuming network availability (via the VPN subnet for a standard workflow or localhost for an “offline” workflow such as those described in the SDS section), the entry point on each host is a DDS, a root-owned top-level directory used as a so-called _chroot jail_ to limit SSH-based access to paths within it:

| directory | user   | group  | permissions |
| --------- | ------ | ------ | ----------- |
| `<dds>`   | `root` | `root` | 755         |

**Default DDS deployment path: `/dds`**

To members of applicable groups, the jail appears over SSH as file system root (i.e., `/`), so additional directories containing the files necessary to provide a functioning login shell for each SDS implementation are bind-mounted from the external system to mount points within the jail.

**Default bind-mounted directories:**

| directory         | bind-mount source | user   | group  | permissions |
| ----------------- | ----------------- | ------ | ------ | ----------- |
| `<dds>/dev`       | `/dev`            | `root` | `root` | 755         |
| `<dds>/nix/store` | `/nix/store`      | ^      | ^      | ^           |
| `<dds>/tmp`       | `/tmp/<dds>/tmp`  | ^      | ^      | ^           |

NOTE: The `/nix/store` entry is included for [Nix]-based platforms.

[TODO: Bind-mounting the device and Nix store directories in their entirety is excessive for the default implementation; `borg` and dependencies for BSDS, `git` and dependencies for GSDS, and `/dev/null` for both likely suffice.]

[nix]: https://nixos.org/
