# GR's

As with all files on a given host, GR names within a given DDS are entirely at the discretion of the host’s administrator. However, conforming to a convention for naming and organizing the most commonly utilized GR's—particularly those shared with visitors—will assist all participants in their efforts to contribute by reinforcing a familiar structure.

NOTE: Working tree GR's are found only on GSDS's (as opposed to BR's, which are found only on BSDS's).
