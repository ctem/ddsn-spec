# Groups

**The default implementation provisions the following group type names:**

| group type name | associated SDS implementations |
| --------------- | ------------------------------ |
| `maintainers`   | BSDS                           |
| `contributors`  | ^                              |
| `visitors`      | BSDS, GSDS                     |

As group types represent access levels to a given org (and its repositories by default[^inheritance]), group type provisioning is SDS implementation-dependent. Please see the corresponding sections for details.

[^inheritance]: By default, contents of each org recursively inherit group ownership (with the gid bit) and all permissions (with Default ACL entries) of the org itself. (WARNING: This inheritance is implemented as a convenience to administrators but is not a substitute for responsible administration; uncaught edge cases may expose sensitive data to unprivileged users.)

Group member lists are both SDS implementation-dependent, and in the case of GSDS's, host-dependent as well. Please see the corresponding sections for assignment tables of PUA's to groups.
