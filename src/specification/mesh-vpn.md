# Mesh VPN

A mesh VPN provides a secure, convenient foundation for a given DDSN.

**Default implementation: [Nebula]**

[TODO: Provide recommendations for NAT traversal in partial mesh deployments; Nebula [needs help][issue].]

[nebula]: https://github.com/slackhq/nebula/
[issue]: https://github.com/slackhq/nebula/issues/33
