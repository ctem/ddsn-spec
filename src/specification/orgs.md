# Orgs

Within each SDS, each org directory is assigned a DDS-provisioned group of one or more group types. Each group’s name contains the group type and the name of the org:

| directory           | user    | group                    | permissions                |
| ------------------- | ------- | ------------------------ | -------------------------- |
| `<dds>/<sds>/<org>` | `<aua>` | `dds-<group type>-<org>` | `u+rwx` `g+r(w?)x` `o-rwx` |

[TODO: Confirm that no access for others (i.e., `o-rwx`) isn’t problematic for normal (non-DDS) operations. If it is, consider refactoring to create for each visitor group its own chroot jail with only the appropriate org(s) bind-mounted in from the AUA's working cop(y/ies). This would allow default permissions to be more relaxed.]

The schema places no limit on the number of orgs that can be provisioned for use within a given DDSN. For maintainability, however, IPC-strictness is recommended where applicable:

- An IPC may be assigned to up to one org (or BSDS org in the case of a BSDS/GSDS org pair).
- Orgs with _coincidentally_ matching participant configurations are considered to be often unavoidable—particularly during periods of incomplete PUA provisioning—and as such are overlooked for an unspecified grace period. However, as a measure to prevent the unnecessary addition of orgs (and associated maintenance cost increase) due to “brand syndrome” and other forms of accidental org misappropriation, action will be taken if multiple orgs (or BSDS orgs in the case of that BSDS/GSDS org pairs are deployed) are suspected of sharing an IPC.
