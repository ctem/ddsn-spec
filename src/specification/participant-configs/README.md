# Participant Configurations

Participant configurations are invariably SDS implementation-dependent; the components that comprise a complete participant configuration are determined by the implementation of the SDS that contains the org to which a given participant configuration is applied.
