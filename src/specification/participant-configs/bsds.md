# BSDS Participant Configurations

By default, a BSDS enforces a _maintainer-prune/contributor-append/visitor-extract_ access pattern:

| group type     | permissions                       | shell                               |
| -------------- | --------------------------------- | ----------------------------------- |
| `maintainers`  | read/write, BR full access        | `borg serve` (`borg` commands only) |
| `contributors` | read/write, BR append-only        | ^                                   |
| `visitors`     | read-only (+ read/write BR roots) | ^                                   |

Host-specific contents are determined for each BSDS:

| org     | BR keyholders  | maintainers       | contributors      | visitors          |
| ------- | -------------- | ----------------- | ----------------- | ----------------- |
| `<org>` | \<participant> | `m-<participant>` | `c-<participant>` | `v-<participant>` |

Notes regarding provisioned group types:

- `maintainers`
  - Special-use; by convention, accounts belonging to this group are used only to perform irregular and/or destructive operations on BR's (e.g., archive pruning).
  - This is a critical group type for the use case of a BR maintainer who is not a system administrator (and therefore otherwise lacks an account with the privileges necessary to perform maintenance operations on BR's).
- `contributors`
  - Regular-use; by convention, accounts belonging to this group are used only to create archives (i.e., with `borg create`).
  - TIP: Use direnv to update `$BORG_REPO` on the fly for a seamless workflow.
- `visitors`
  - Regular-use; by convention, accounts belonging to this group are used only to access BR's as special remotes via git-annex (e.g., with `git annex sync`).
  - This group type provides write access to BR root directories to allow all members to write a lock file. However, access to existing files within BR root directories other than lock files remains read-only.
  - In the default implementation, even holders of escalated-privilege accounts are expected to use a visitor account as a means to adhere to the principle of least privilege for common read-only use cases.

The BSDS has the unique task of supporting distributed access to BR's despite the fact that Borg is not designed to accommodate a multi-user workflow robustly. There are several considerations:

- A critical component of every BR is its lock file, which designates whether the BR is currently in use. Even visitors performing read-only operations (e.g., checks and extractions) require read/write access to the BR's top-level directory because they must be able to write the lock file to prevent errors and corruption resulting from attempted reads of BR data that are being modified concurrently by another process.
- Each BR can be encrypted with up to one key, which must be shared with every user to whom access (whether read, write, or both) to the BR's data is to be granted.
- [TODO: Restrict access to certain subcommands based on group type?]
- [TODO: How to handle initial BR propagation to new hosts?]

Notes:

- To mitigate BR “lock blocking”: for each of a given host’s BR's of which the server’s primary administrator (PA) is one of multiple contributors, the PA is encouraged to be the sole contributor of the BR to the extent of reasonable use.
- To access a given BR, all account types (even visitors) require the BR key.
- BR's are stored in their respective org directories, which gives server administrators the option to set ACLs to hide org contents (i.e., all the org’s BR's) from any PAU or group thereof. (This opposes standard practice, however, as all peers are encouraged to store a copy of all BR's of all orgs, regardless of individual BR key distribution.)
- Should automated syncing among peers be implemented in the future, each host will be assigned its own visitor account to allow event-based pulls (e.g., via a queue); write operations will be performed locally (i.e., no remote pushes).
