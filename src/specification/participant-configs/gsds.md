# GSDS Participant Configurations

By default, a GSDS enforces a _visitor-pull_ access pattern:

| group type | permissions | shell                             |
| ---------- | ----------- | --------------------------------- |
| `visitors` | read-only   | `git-shell` (`git` commands only) |

Host-specific contents are determined for each GSDS:

| org     | visitors          |
| ------- | ----------------- |
| `<org>` | `v-<participant>` |

Notes:

- In accordance with the visitor-pull pattern, SSH access to all GSDS contents for DDS-provisioned (i.e., non-administrative) accounts is read-only without exception. For administrators, write-level access with an AUA is assumed. Therefore, there is no use case for provisioning any group type allowing write access.
- Although an administrator of (e.g.) two workstations could access one from the other over SSH with an AUA, the administrator may nevertheless prefer instead to use a visitor account to follow the prescribed visitor-pull pattern, thereby adhering to the principle of least privilege. The tables address this use case by including:
  - the visitor account of the administrator of a given GSDS as a member of each of the GSDS's visitor groups
  - a special owner-dedicated visitor group for read-only access to the administrator’s private org
