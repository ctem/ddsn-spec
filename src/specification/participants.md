# Participants

Each participant of a given DDSN is assigned an alias that is used (typically along with respective group type names) to derive the names of all PUA's provisioned to that participant.

Tentative PUA naming convention: `<first letter of group type>-<participant>`
