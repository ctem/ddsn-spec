# SDS's

A DDS is divided into SDS's based on repository format. SDS's provide the basis for repository format-specific environment security. Due to this tight coupling, each SDS is named after its associated repository format. This implies that a given DDS is limited to one SDS per utilized repository format, which is considered a sane default. (Note, however, that the naming is arbitrary, allowing the flexibility to provision multiple SDS's associated with a common repository format if desired.)

| directory     | user   | group  | permissions |
| ------------- | ------ | ------ | ----------- |
| `<dds>/<sds>` | `root` | `root` | 755         |

Conceptually, a given SDS is considered to be either _archive-designated_ or _work-designated_. **The default implementations for these designations are the BSDS and GSDS, respectively:**

| designation | >                  | >             | defaults                            |
| ----------- | ------------------ | ------------- | ----------------------------------- |
| **-**       | **implementation** | **directory** | **repository format**               |
| “archive”   | BSDS               | `<dds>/borg`  | BR's                                |
| “work”      | GSDS               | `<dds>/git`   | GR's (typically with working trees) |

In a typical DDSN:

- A single DDS is deployed to each node.
  - The DDS deployed to each headless node contains a single BSDS.
  - The DDS deployed to each workstation node contains a single GSDS.

Note that the specification affords the flexibility to customize deployments per use case, thereby accommodating unique workflows. Examples:

- “offline laptop” (a.k.a. “minimalist nomad”): an administrator of a laptop with limited storage capacity and sporadic internet connectivity may find it convenient to deploy to the laptop a DDS containing both a GSDS and a BSDS. This allows the administrator to conserve storage by moving momentarily unneeded data into a BR (thereby taking advantage of Borg’s compression and deduplication features), and when the data is needed, extract it from the BR and return it to its original GR.
- “offline battlestation” (a.k.a. “antisocial hoarder”): an administrator of an air-gapped workstation with generous storage capacity may find it convenient to deploy to the laptop a DDS containing both a GSDS and a BSDS. This allows the administrator to keep ample local backups in BR's (thereby taking advantage of Borg’s data consistency features) that at any time can be verified for consistency, analyzed for forensic purposes, or extracted for recovery.
